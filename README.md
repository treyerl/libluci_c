# README #

This is the C++ library to connect and communicate with luci2.

### Where to find the actual library ###

The library is located in include/luciCon/luciCon.h along the other necessary includes in include/

### How to start ###

There is a documented example file called example.cpp in the repository. This example service recieves a picture and a textfile, stores them locally and sends them back to luci.
For the necessary compiling flag please see the provided Makefile.