/*
 *
 * Add some example here and description. An example for creating a service.
 *
 * Created by Dani Zünd 2015
 * Adapted to luci2 and extended by Alessandro Forino 2016
 *
 */

#ifndef __LUCICON_INCLUDED__
#define __LUCICON_INCLUDED__

#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <map>
#include <istream>
#include <ostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <signal.h>
#include "md5/md5.h"
#include <boost/algorithm/string.hpp>
#include "../rapidjson/document.h"
#include <boost/bind.hpp>
#include <boost/optional.hpp>

#include <fstream>
#include <boost/lexical_cast.hpp>
#include "base64/base64.h"
#include <random>

using namespace std;

namespace luci {
	class LuciConnect {
		protected:
			boost::asio::io_service ioService;
			boost::asio::ip::tcp::socket* socket;
			boost::asio::ip::tcp::resolver::iterator iterator; // resolving some connection data
			rapidjson::Document inputDoc;
			std::map<std::string, std::string> outputTypes;
			std::map<std::string, std::string> inputTypes;
			std::vector<std::string> dataStreams;
			std::vector<std::string> dataStreamNames;
			std::vector<size_t> dataStreamLenghts;

			boost::array<char, 8> async_hLenght;
			boost::array<char, 8> async_aLenght;
			boost::asio::streambuf async_header;
			bool canceled = false;

		protected:
			string sendAndReceiveOnlyJson(string);
			string sendAndReceiveFull(string, string);

		protected:
			void loadJson(string);
			bool processStreams(string, string, uint64_t);
			uint64_t byteswap64(uint64_t);
			uint64_t charToUint64(const char*);
			void uint64ToChar(uint64_t, char*);

		protected:
			bool sendOnlyJson(string);
			bool sendUint64(uint64_t);
			bool sendSizes(uint64_t, uint64_t);
			bool sendString(string);
			bool sendAtt(string);
			bool sendFull(string, string);

		protected:
			string receiveMsg();
			uint64_t receiveUint64();
			string getNBytes(int);
			void recieveAsync();
			void handleReadHeaderLength(const boost::system::error_code& err);
			void handleReadAttachmentLength(const boost::system::error_code& err);
			void handleCheckCancel(const boost::system::error_code& err);

		protected:
			void panic_recovery_procedure();

		public:
			// Set up the connection to Luci
			//
			// input:
			//          url ... adress of Luci [string]
			//          port ... port number [string]
			//
			// output:
			//          true if successful
			//          false if not sucessful
			bool connect(std::string, std::string);

			// Disconnect from Luci
			//
			// output:
			//          true if successful
			//          false if not sucessful
			bool disconnect();

			// Authenticate user
			//
			// input:
			//          user ... username [string]
			//          pw .. password [string]
			bool authenticate(std::string, std::string);


			// Register a remote_service for Luci
			//
			// input:
			//          serviceName ... name of the remote_service [string]
			//          machinename ... name of the computer [string]
			//          description ... description of what the service does [string]
			//          version     ... version of the service [string]
			//          inputNames  ... names of the input parameters [vector<string>]
			//          inputTypes  ... types of the input parameters [vector<string>]
			//          optional    ... describes if an input is optional [vector<bool>]
			//          outputNames ... names of the outputs [vector<string>], if
			//                          streaminfo, add the type after a slash,
			//                          e.g. streaminfo/png when it is a png
			//                          picture.
			//          outputTypes ... types of the outputs [vector<string>]
			//
			// output:
			//          true if successful
			//          false if not sucessful
			bool registerService(std::string, std::string,
					std::vector<std::string>, std::vector<std::string>, std::vector<bool>,
					std::vector<std::string>, std::vector<std::string>
					);

			// waits for task messages of Luci
			//
			// output:
			//          json string of the input
			std::string waitForTask();

			// checks if task was canceled
			//
			// output:
			//          bool true if canceled, false if not canceled
			bool isCanceled();

			// sends a result to Luci
			//
			//  input:
			//          resultNames ... the names of the results [string]
			//          resultValues... the corresponding values to the names in
			//                          resultNames [string]
			//          intermediate... if it is an end result or an intermediate one [bool]
			//			progress...		the progress in percentage [int]
			void sendResult(std::vector<std::string>, std::vector<std::string>, bool = false, int = 0);

			// Please use this method just for iterating purposes
			// Use getStreamData(std::string) if you need a specific stream
			// (streams NOT sorted!)
			//
			// returns the results of the data streams that came in after the main task message
			//
			//  input:
			//          number ... the number of the data stream [int]
			//
			//  output:
			//          pointer to the string (can be seen as char array) of the stream data
			std::string* getStreamData(int);

			// returns the results of the data streams that came in after the main task message
			//
			//  input:
			//          name ... the name of the data stream [string]
			//
			//  output:
			//          pointer to the string (can be seen as char array) of the stream data
			std::string* getStreamData(std::string);

			//std::string getStreamName(std::string);
			//std::string getStreamName(int);
	};
};

namespace luci {
	using namespace std;
	using boost::asio::ip::tcp;

	string LuciConnect::waitForTask() {
		this->socket->cancel();
		this->canceled = false;

		cout << "Waiting for Task ..." << flush;
		string ret =  this->receiveMsg();
		if(ret == "") {
			return waitForTask();
		}
		cout  << " got one!" << endl;
		recieveAsync();

		return ret;
	}

	void LuciConnect::sendResult(vector<string> names, vector<string> values, bool intermediate, int progress) {
		// Stream to send JSON
		ostringstream ss;

		// Stream for checksum calculation
		ostringstream ss1;

		// For calculating and storing the checksums
		vector<string> checksum;

		// Size of the bytearray
		uint64_t filesize;

		// To calculate checksum
		MD5 md5;

		bool has_attachments;

		//order in case streams are sent
		int position = 1;

		//calculating checksums
		for (int k = 0; k<values.size(); ++k){
			ss1.str("");                            // Clearing the stream
			ss1<<values[k];                         // Loading the byte array into the stream
			checksum.push_back(md5(ss1.str()));
			boost::to_upper(checksum[k]);
		}
		string strI = "attachment";
		string outStream = "";
		string outStreamFull = "";
		//Writing the JSON String
		ss << "{";
		ss << (intermediate?"'progress'":"'result'");
		ss << ":{";
		ss <<  "'outputs':{";
		for (int i = 0; i < values.size(); ++i) {
			ss << "'"<< names[i] << "':";

			//checks if streaminfo, string or number as output type
			if (outputTypes[names[i]].compare("string") == 0) {
				ss << "'" << values[i] << "'";
			} else if (outputTypes[names[i]].compare(strI) >= 0){
				has_attachments = true;
				ss<<"{'format' : '" << outputTypes[names[i]].substr(strI.length()+1, string::npos) << "', ";
				ss<<"'attachment': { 'checksum' : '"<<checksum[i]<<"', ";
				ss<<"'position' : " << position << ", ";
				ss<<"'length' : " << values[i].size() << "}}";
				position++;

				//insert lenght of the attachment before every attachment
				char aLen[sizeof(uint64_t)];
				uint64ToChar(values[i].size(), aLen);
				outStream.append(aLen,sizeof(uint64_t));
				outStream.append(values[i]);
			} else {
				ss << values[i];
			}
			ss << (i+1<values.size()?",":"");
		}
		ss << "}}";
		ss << (intermediate?(",'percentage':"+std::to_string(progress)+"}"):"}");

		//if attachments are sent, add number of attachments at the beginning of attachment-stream
		if(has_attachments) {
			char numA[sizeof(uint64_t)];
			uint64ToChar((position-1), numA);
			outStreamFull.append(numA, sizeof(uint64_t));
			outStreamFull.append(outStream);
		}

		sendFull(ss.str(), outStreamFull);
	};

	bool LuciConnect::connect(string url, string port) {
		cout << "Connecting ... ";
		tcp::resolver resolver(this->ioService);
		tcp::resolver::query query(url, port);
		this->iterator = resolver.resolve(query);
		this->socket = new tcp::socket(this->ioService);
		try {
			boost::asio::connect(*(this->socket), iterator);
			boost::asio::socket_base::keep_alive option(true);
			this->socket->set_option(option);
		} catch (std::exception& e) {
			std::cerr << "\nException: " << e.what() << endl;
			return 0;
		};
		cout << "Done" << endl;
		return 1;
	}

	bool LuciConnect::disconnect() {
		try {
			cout << "Closing connection ...";
			this->socket->close();
			delete this->socket;
		} catch (std::exception& e) {
			std::cerr << "\nException: " << e.what() << endl;
			return 0;
		};
		cout << "Done" << endl;
		return 1;
	};

	bool LuciConnect::authenticate(string user, string pw) {
		cout << "Authenticating: " << user << " ... ";
		ostringstream ss;
		ss << "{\"action\":\"authenticate\", \"username\":\"" << user
			<< "\", \"userpasswd\":\"" << pw << "\"}\n";
		string respond = this->sendAndReceiveOnlyJson(ss.str());
		if (respond.find("authenticated") != string::npos) {
			cout << "Done" << endl;
			return 1;
		}
		return 0;
	};

	bool LuciConnect::registerService(string servName, string exampleCall,
			vector<string> inpNames, vector<string> inpTypes, vector<bool> opt,
			vector<string> outNames, vector<string> outTypes
			) {
		cout << "Registering: " << servName << " ... ";

		for (int i = 0; i < outNames.size(); ++i)
			this->outputTypes[outNames[i]] = outTypes[i];
		for (int i = 0; i < inpNames.size(); ++i)
			this->inputTypes[inpNames[i]] = inpTypes[i];
		ostringstream ss;
		ss << "{\"run\":\"RemoteRegister\", ";
		ss << "\"serviceName\":\"" << servName << "\", ";
		ss << "\"exampleCall\":" << exampleCall << ", "; //, ";
		ss << "\"inputs\":{ ";
		for (int i = 0; i < inpNames.size(); ++i) {
			ss << "\"" << (opt[i]?"OPT ":"")
				<< inpNames[i] << "\":\"" << inpTypes[i] << "\""
				<< ((i+1<inpNames.size())?", ":" ");
		};
		ss << "},";
		ss << "\"outputs\":{ ";
		string strI = "streaminfo";
		for (int i = 0; i < outNames.size(); ++i) {
			string type;
			if (outTypes[i].compare(0, strI.size(), strI)) {
				type = outTypes[i];
			} else {
				type = "streaminfo";
			}
			ss << "\"" << outNames[i] << "\":\"" << type << "\""
				<< ((i+1<outNames.size())?", ":" ");
		};
		ss << "}}";
		ss.clear();


		sendOnlyJson(ss.str());

		receiveMsg();
		receiveMsg();
		string respond = receiveMsg();

		if (respond.find("result") != string::npos) {
			cout << "Done" << endl << flush;
			return 1;
		}

		return 0;
	}

	bool LuciConnect::isCanceled() {
		ioService.poll();
		return this->canceled;
	}

	string* LuciConnect::getStreamData(int index) {
		if(dataStreams.empty()) {
			cout << "Data has no elements";
			return NULL;
		}
		else return &(dataStreams[index-1]);
	}

	string* LuciConnect::getStreamData(string name) {
		int i = 0;
		//boost::algorithm::to_lower(name);
		for (;i < dataStreamNames.size(); ++i) {
			if (!name.compare(dataStreamNames[i])) {
				break;
			}
		}
		if(dataStreams.empty()) {
			cout << "Data has no elements";
			return NULL;
		}
		else return &(dataStreams[i]);
	}

	/********************************/
	/*      private fcts            */
	/********************************/

	string LuciConnect::sendAndReceiveOnlyJson(string msg) {
		this->sendOnlyJson(msg);
		string out = this->receiveMsg();
		/* cout << out << endl; */
		return out;
	}

	string LuciConnect::sendAndReceiveFull(string header, string att) {
		this->sendFull(header, att);
		string out = this->receiveMsg();
		return out;
	}

	/*
	 * Receiving
	 */

	string LuciConnect::receiveMsg() {
		uint64_t hLen = this->receiveUint64();
		//    cout << hLen << " | ";
		uint64_t attLen = this->receiveUint64();
		//    cout << attLen << " | ";
		string header = this->getNBytes((int) hLen);
		//    cout << header << endl;
		string att = this->getNBytes((int) attLen);
		//    cout << att << endl;
		if(this->processStreams(header, att, attLen)) {
			return header;
		}
		return "";
	}

	uint64_t LuciConnect::receiveUint64() {
		uint64_t out;
		string s;
		const char* c;
		s = this->getNBytes(sizeof(uint64_t));
		c = s.c_str();
		out = this->charToUint64(c);
		return out;
	}

	string LuciConnect::getNBytes(int s) {
		vector<char> buf(s);
		boost::system::error_code error;
		try {
			boost::asio::read(*(this->socket), boost::asio::buffer(buf));
			if (error == boost::asio::error::eof) {
				this->disconnect();
				exit(0);
			};
		} catch (std::exception& e) {
			cerr << "\nException: " << e.what() << endl;
		}
		string res(buf.begin(), buf.end());
		return res;
	}

	void LuciConnect::recieveAsync() {
		boost::asio::async_read(*(this->socket), boost::asio::buffer(async_hLenght),
				boost::asio::transfer_exactly(8),
				boost::bind(&LuciConnect::handleReadHeaderLength, this, boost::asio::placeholders::error));
	}

	void LuciConnect::handleReadHeaderLength(const boost::system::error_code& err) {
		if(!err) {
			boost::asio::async_read(*(this->socket), boost::asio::buffer(async_aLenght),
					boost::asio::transfer_exactly(8),
					boost::bind(&LuciConnect::handleReadAttachmentLength, this, boost::asio::placeholders::error));
		}
		else {
			std::cerr << "Error occurred." << std::endl;
			this->disconnect();
			exit(0);
		}
	}

	void LuciConnect::handleReadAttachmentLength(const boost::system::error_code& err) {
		if(!err) {
			uint64_t hLenght = charToUint64(async_hLenght.data());
			boost::asio::async_read(*(this->socket), async_header,
					boost::asio::transfer_exactly(hLenght),
					boost::bind(&LuciConnect::handleCheckCancel, this, boost::asio::placeholders::error));
		}
		else {
			std::cerr << "Error occurred." << std::endl;
			this->disconnect();
			exit(0);
		}
	}

	void LuciConnect::handleCheckCancel(const boost::system::error_code& err) {
		if(!err) {
			std::string cancel_string((istreambuf_iterator<char>(&async_header)), istreambuf_iterator<char>());
			if (cancel_string.find("cancel") != string::npos) {
				cout << "Cancelled!" << endl << flush;
				this->canceled = true;
			}
		}
		else {
			std::cerr << "Error occurred." << std::endl;
			this->disconnect();
			exit(0);
		}
	}

	/*
	 * Sending
	 */

	bool LuciConnect::sendFull(string header, string att) {
		uint64_t dLen = header.size();
		uint64_t attLen = att.size();

		//    cout << dLen << endl;
		//    cout << attLen << endl;

		bool good;
		/* cout << "-------------------------------" << endl; */
		/* cout << dLen << " | " << attLen << endl; */
		/* cout << "###############################" << endl; */
		/* cout << header << endl; */
		good = this->sendSizes(dLen, attLen);
		good &= this->sendString(header);
		good &= this->sendString(att);
		return good;
	}

	bool LuciConnect::sendOnlyJson(string in) {
		uint64_t dLen = in.size();
		uint64_t attLen = 0;
		bool good;
		good = this->sendUint64(dLen);
		good &= this->sendUint64(attLen);
		good &= this->sendString(in);
		return good;
	}

	bool LuciConnect::sendString(string in) {
		const char* data = in.c_str();
		try {
			this->socket->set_option(tcp::no_delay(true));
			boost::asio::write(*(this->socket), boost::asio::buffer(data, in.size()));
		} catch (std::exception& e) {
			std::cerr << "\nException: " << e.what() << "\n";
			return 0;
		};
		return 1;
	}

	bool LuciConnect::sendSizes(uint64_t hSize, uint64_t attSize) {
		bool good;
		good = this->sendUint64(hSize);
		good &= this->sendUint64(attSize);
		return good;
	}

	bool LuciConnect::sendUint64(uint64_t inNum) {
		try {
			uint64_t num = this->byteswap64(inNum);
			this->socket->set_option(tcp::no_delay(true));
			boost::asio::write(*(this->socket), boost::asio::buffer(&num, sizeof(uint64_t)));
		} catch (exception& e) {
			std::cerr << "\nException: " << e.what() << endl;
			return 0;
		}
		return 1;
	}

	bool LuciConnect::sendAtt(string att) {
		bool good;
		good = this->sendString(att);
		return good;
	}

	/*
	 * Converters
	 */

	bool LuciConnect::processStreams(string header, string data, uint64_t attLen) {
		this->loadJson(header);
		if (charToUint64(data.substr(0,7).c_str()) == 0) return true;
		dataStreams.clear();
		dataStreamNames.clear();
		vector<string> names, formats, checksums;
		vector<int> lengths, positions;

		for (map<string, string>::iterator it = inputTypes.begin();
				it != inputTypes.end(); ++it) {
			string currStr = it->second;
			boost::algorithm::to_lower(currStr);
			if (strcmp(it->second.c_str(),"attachment") == 0) {
				names.push_back(it->first);
				const rapidjson::Value& currVal = this->inputDoc[it->first.c_str()][it->second.c_str()];

				/* formats.push_back(currVal["format"].GetString()); */
				/* currVal = this->inputDoc["inputs"][it->first]["streaminfo"]; */
				checksums.push_back(currVal["checksum"].GetString());
				lengths.push_back(currVal["length"].GetInt());
				positions.push_back(currVal["position"].GetInt());
			}
		}

		uint64_t total_length = 0;
		uint64_t n = charToUint64(data.substr(0, 7).c_str());
		uint64_t att_n_length = 0;
		uint64_t val_pos = 8; 
			uint64_t next_pos = charToUint64(data.substr(8, 15).c_str());
		for(int i = 1; i <= n; ++i) {
			att_n_length = charToUint64(data.substr(val_pos, 8).c_str());
			total_length += att_n_length;
			val_pos += att_n_length + 8;
		}
		if(((n+1)*8 + total_length) != attLen) {
			panic_recovery_procedure();
			return false;
		}
		else {
			//first 8 bytes number of attachments, next 8 bytes length of first attachment
			int pos = 16;
			int cnt = 1;

			while (pos < data.length()-1) {
				for (int i = 0; i < names.size(); ++i) {
					if (positions[i] == cnt) {
						cnt++;
						dataStreams.push_back(data.substr(pos, lengths[i]));
						dataStreamNames.push_back(names[i]);

						//before every attachment 8 bytes of length of attachment
						pos += lengths[i]+8;
					}
				}
			}
		}

		return true;
	}

	void LuciConnect::loadJson(string input) {
		char* cjson = new char [input.length()+1];
		strcpy(cjson, input.c_str());
		this->inputDoc.Parse(cjson);
		delete[] cjson;
	}

	uint64_t LuciConnect::charToUint64(const char* in) {
		const char* c = in;
		uint64_t tmp;
		memcpy(&tmp, c, sizeof(uint64_t));
		uint64_t out = this->byteswap64(tmp);
		return out;
	}

	void LuciConnect::uint64ToChar(uint64_t in, char* out) {
		uint64_t in_swap = byteswap64(in);
		memcpy(out, &in_swap, sizeof(uint64_t));
	}

	uint64_t LuciConnect::byteswap64(uint64_t in) {
		char num[sizeof(uint64_t)];
		uint64_t res;
		memcpy(num, &in, sizeof(uint64_t));
		reverse(num, num+sizeof(uint64_t));
		memcpy(&res, num, sizeof(uint64_t));

		return res;
	};

	void LuciConnect::panic_recovery_procedure() {
		std::random_device engine;
		uint32_t panicID = engine();

		std::string panicID_str = std::to_string(panicID);

		std::string encoded = base64_encode(reinterpret_cast<const unsigned char*>(panicID_str.c_str()), 32);
		std::string panic_message = "{'panic':" + encoded + "}";
		sendOnlyJson(panic_message);

		std::string response = getNBytes(32);
		std::string decoded = base64_decode(response);

		bool panic = true;
		while(panic) {
			if(decoded == panicID_str) {
				panic = false;	
			}
			else {
				std::string response_new = getNBytes(1);
				response.erase(0, 1);
				response.push_back(response_new.c_str()[0]);
			}
		}
		
		return;
	}
};

#endif


