#include <string>
#include <fstream>
#include "luciCon/luciCon.h"

using namespace std;

int main() {

	//connect to luci
	luci::LuciConnect lucicon;
	if(lucicon.connect("localhost","7654")) {
		
        //initialize service informations
		vector<string> inpNames = {"in1", "in2"};
		vector<string> inpTypes = {"attachment", "attachment"};
		vector<bool> opt = {0, 0};
		vector<string> outNames = {"out1", "out2"};
		vector<string> outTypes = {"attachment/jpg", "attachment/txt"};
		string example_call = "{\"run\":\"luci.service.test_service\",\"in1\":\"picture.jpg\",\"in2\":\"textfile.txt\"}"; 

		//register the service
		lucicon.registerService("luci.service.test_service", example_call, inpNames, inpTypes, opt, outNames, outTypes);

		//service to run
		while(1) {

			//wait for luci to send a task
			lucicon.waitForTask();

			//write the textfile and the picture to a local file
			ofstream file1;
			ofstream file2;
			file1.open("picture.jpg");
			file1.write(lucicon.getStreamData("in1")->c_str(),lucicon.getStreamData("in1")->size());
			file1.flush();
			file2.open("textfile.txt");
			file2.write(lucicon.getStreamData("in2")->c_str(),lucicon.getStreamData("in2")->size());
			file2.flush();
			file1.close();
			file2.close();

			//put the written files into a string for sending back to luci
			std::ifstream out1("picture.jpg");
			std::ifstream out2("textfile.txt");
			std::string str1((std::istreambuf_iterator<char>(out1)),
							 std::istreambuf_iterator<char>());
			std::string str2((std::istreambuf_iterator<char>(out2)),
							 std::istreambuf_iterator<char>());
			vector<string> outValues = {str1, str2};

			//send an intermediate result
			lucicon.sendResult(outNames, outValues, true, 50);

			//check if luci is canceled (usually used inside a loop to cancel the task)
			sleep(10);
			if(lucicon.isCanceled()) {
				break;
			}

			//send the final result
			lucicon.sendResult(outNames, outValues);
		}

		//disconnect service
		lucicon.disconnect();
	}
	return 0;
}

