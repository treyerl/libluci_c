CXX=g++
CXXFLAGS=-c -Wall -std=c++11
LDFLAGS=-lboost_system -pthread
INCLUDE=include

all: luci_example

luci_example: include/base64/base64.o example.o
	$(CXX) $(LDFLAGS) -I$(INCLUDE) example.o base64.o -o luci_example

example.o: example.cpp
	$(CXX) $(CXXFLAGS) -I$(INCLUDE) example.cpp

include/base64/base64.o: include/base64/base64.cpp
	$(CXX) $(CXXFLAGS) -I$(INCLUDE) include/base64/base64.cpp

clean: 
	rm ./*.o luci_example
